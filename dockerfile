FROM golang
ENV GO111MODULE=on
ENV GOPROXY="https://goproxy.io"
WORKDIR $GOPATH/src/service-a
ADD . $GOPATH/src/service-a
RUN git clone https://gitlab.cn/jihulab/jh-infra/hands-on-tests/service-a.git $GOPATH/src/service \
    && go build . 
EXPOSE 8888
ENTRYPOINT ["./service-a","--f","config.yaml"]
